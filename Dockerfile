FROM pandoc/core

WORKDIR /app

RUN echo "Start run" \ 
	&& apk add --update nodejs npm \
	&& npm install --save pandoc-doi2bib \
	&& npm list

ENV d2b="/app/node_modules/pandoc-doi2bib/index.js"

ENTRYPOINT pandoc --filter $d2b --citeproc